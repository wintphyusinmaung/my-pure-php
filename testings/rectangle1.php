<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    require('Rectangle.php');

    $width = 50;
    $height = 50;

     echo "<h2>With a width of $width and a height of $height...</h2>";

     $r = new Rectangle($width,$height);

    echo '<p>The area of the rectangel is ' . $r->getArea() . '</p>';

    echo '<p>The perimeter of the rectangle is ' . $r->getPerimeter() . '</p>';

    echo '<p>This rectangle is ';
    if ($r->isSquare()){
        echo 'also';
    }else{
        echo 'not';
    }

    echo ' a square.</p>';

    unset($r);

    
    ?>

</body>
</html>