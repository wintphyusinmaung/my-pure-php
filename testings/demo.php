<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php  
class Demo {
    function __construct()
    {
        echo '<p>In the constructor.</p>';
    }

    function __destruct()
    {
        echo '<p>In the destructor.</p>';
    }

}

function test()
{
    echo '<p>In the function.
    Creating a new object...</p>';

    $f = new Demo();
    echo '<p>About to leave the function.</p>';
}

echo '<p>Creating a new object...</p>';//first

$o = new Demo();
test();

echo '<p>Calling the function...</p>'


?>

</body>
</html>